﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using EcuationSystemWF.View;
using EcuationSystemWF.Model;
using EcuationSystemWF.Presenter;

namespace EcuationSystemWF
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var mainForm = new MainView();
            var model = new VectorSystem();
            var presenter = new MainPresenter(mainForm, model);
            Application.Run(mainForm);
        }
    }
}
