﻿namespace EcuationSystemWF
{
    partial class EcuationCalculator
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.txtBoxX1 = new MetroFramework.Controls.MetroTextBox();
            this.txtBoxY1 = new MetroFramework.Controls.MetroTextBox();
            this.txtBoxZ1 = new MetroFramework.Controls.MetroTextBox();
            this.txtBoxZ2 = new MetroFramework.Controls.MetroTextBox();
            this.txtBoxY22 = new MetroFramework.Controls.MetroTextBox();
            this.txtBoxX2 = new MetroFramework.Controls.MetroTextBox();
            this.txtBoxZ3 = new MetroFramework.Controls.MetroTextBox();
            this.txtBoxY3 = new MetroFramework.Controls.MetroTextBox();
            this.txtBoxX3 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.txtBoxResult3 = new MetroFramework.Controls.MetroTextBox();
            this.txtBoxResult2 = new MetroFramework.Controls.MetroTextBox();
            this.txtBoxResult1 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.txtLabelZ = new MetroFramework.Controls.MetroLabel();
            this.txtLabelY = new MetroFramework.Controls.MetroLabel();
            this.txtLabelX = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(76, 284);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(142, 41);
            this.metroButton1.TabIndex = 0;
            this.metroButton1.Text = "Resolver";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.MetroButton1_Click);
            // 
            // txtBoxX1
            // 
            // 
            // 
            // 
            this.txtBoxX1.CustomButton.Image = null;
            this.txtBoxX1.CustomButton.Location = new System.Drawing.Point(30, 1);
            this.txtBoxX1.CustomButton.Name = "";
            this.txtBoxX1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoxX1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoxX1.CustomButton.TabIndex = 1;
            this.txtBoxX1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoxX1.CustomButton.UseSelectable = true;
            this.txtBoxX1.CustomButton.Visible = false;
            this.txtBoxX1.Lines = new string[] {
        "X1"};
            this.txtBoxX1.Location = new System.Drawing.Point(76, 117);
            this.txtBoxX1.MaxLength = 32767;
            this.txtBoxX1.Name = "txtBoxX1";
            this.txtBoxX1.PasswordChar = '\0';
            this.txtBoxX1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoxX1.SelectedText = "";
            this.txtBoxX1.SelectionLength = 0;
            this.txtBoxX1.SelectionStart = 0;
            this.txtBoxX1.ShortcutsEnabled = true;
            this.txtBoxX1.Size = new System.Drawing.Size(52, 23);
            this.txtBoxX1.TabIndex = 1;
            this.txtBoxX1.Text = "X1";
            this.txtBoxX1.UseSelectable = true;
            this.txtBoxX1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoxX1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBoxX1.Click += new System.EventHandler(this.TxtBoxOnClick);
            // 
            // txtBoxY1
            // 
            // 
            // 
            // 
            this.txtBoxY1.CustomButton.Image = null;
            this.txtBoxY1.CustomButton.Location = new System.Drawing.Point(30, 1);
            this.txtBoxY1.CustomButton.Name = "";
            this.txtBoxY1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoxY1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoxY1.CustomButton.TabIndex = 1;
            this.txtBoxY1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoxY1.CustomButton.UseSelectable = true;
            this.txtBoxY1.CustomButton.Visible = false;
            this.txtBoxY1.Lines = new string[] {
        "Y1"};
            this.txtBoxY1.Location = new System.Drawing.Point(158, 117);
            this.txtBoxY1.MaxLength = 32767;
            this.txtBoxY1.Name = "txtBoxY1";
            this.txtBoxY1.PasswordChar = '\0';
            this.txtBoxY1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoxY1.SelectedText = "";
            this.txtBoxY1.SelectionLength = 0;
            this.txtBoxY1.SelectionStart = 0;
            this.txtBoxY1.ShortcutsEnabled = true;
            this.txtBoxY1.Size = new System.Drawing.Size(52, 23);
            this.txtBoxY1.TabIndex = 2;
            this.txtBoxY1.Text = "Y1";
            this.txtBoxY1.UseSelectable = true;
            this.txtBoxY1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoxY1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBoxY1.Click += new System.EventHandler(this.TxtBoxOnClick);
            // 
            // txtBoxZ1
            // 
            // 
            // 
            // 
            this.txtBoxZ1.CustomButton.Image = null;
            this.txtBoxZ1.CustomButton.Location = new System.Drawing.Point(30, 1);
            this.txtBoxZ1.CustomButton.Name = "";
            this.txtBoxZ1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoxZ1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoxZ1.CustomButton.TabIndex = 1;
            this.txtBoxZ1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoxZ1.CustomButton.UseSelectable = true;
            this.txtBoxZ1.CustomButton.Visible = false;
            this.txtBoxZ1.Lines = new string[] {
        "Z1"};
            this.txtBoxZ1.Location = new System.Drawing.Point(240, 117);
            this.txtBoxZ1.MaxLength = 32767;
            this.txtBoxZ1.Name = "txtBoxZ1";
            this.txtBoxZ1.PasswordChar = '\0';
            this.txtBoxZ1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoxZ1.SelectedText = "";
            this.txtBoxZ1.SelectionLength = 0;
            this.txtBoxZ1.SelectionStart = 0;
            this.txtBoxZ1.ShortcutsEnabled = true;
            this.txtBoxZ1.Size = new System.Drawing.Size(52, 23);
            this.txtBoxZ1.TabIndex = 3;
            this.txtBoxZ1.Text = "Z1";
            this.txtBoxZ1.UseSelectable = true;
            this.txtBoxZ1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoxZ1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBoxZ1.Click += new System.EventHandler(this.TxtBoxOnClick);
            // 
            // txtBoxZ2
            // 
            // 
            // 
            // 
            this.txtBoxZ2.CustomButton.Image = null;
            this.txtBoxZ2.CustomButton.Location = new System.Drawing.Point(30, 1);
            this.txtBoxZ2.CustomButton.Name = "";
            this.txtBoxZ2.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoxZ2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoxZ2.CustomButton.TabIndex = 1;
            this.txtBoxZ2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoxZ2.CustomButton.UseSelectable = true;
            this.txtBoxZ2.CustomButton.Visible = false;
            this.txtBoxZ2.Lines = new string[] {
        "Z2"};
            this.txtBoxZ2.Location = new System.Drawing.Point(240, 167);
            this.txtBoxZ2.MaxLength = 32767;
            this.txtBoxZ2.Name = "txtBoxZ2";
            this.txtBoxZ2.PasswordChar = '\0';
            this.txtBoxZ2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoxZ2.SelectedText = "";
            this.txtBoxZ2.SelectionLength = 0;
            this.txtBoxZ2.SelectionStart = 0;
            this.txtBoxZ2.ShortcutsEnabled = true;
            this.txtBoxZ2.Size = new System.Drawing.Size(52, 23);
            this.txtBoxZ2.TabIndex = 6;
            this.txtBoxZ2.Text = "Z2";
            this.txtBoxZ2.UseSelectable = true;
            this.txtBoxZ2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoxZ2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBoxZ2.Click += new System.EventHandler(this.TxtBoxOnClick);
            // 
            // txtBoxY22
            // 
            // 
            // 
            // 
            this.txtBoxY22.CustomButton.Image = null;
            this.txtBoxY22.CustomButton.Location = new System.Drawing.Point(30, 1);
            this.txtBoxY22.CustomButton.Name = "";
            this.txtBoxY22.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoxY22.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoxY22.CustomButton.TabIndex = 1;
            this.txtBoxY22.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoxY22.CustomButton.UseSelectable = true;
            this.txtBoxY22.CustomButton.Visible = false;
            this.txtBoxY22.Lines = new string[] {
        "Y2"};
            this.txtBoxY22.Location = new System.Drawing.Point(158, 167);
            this.txtBoxY22.MaxLength = 32767;
            this.txtBoxY22.Name = "txtBoxY22";
            this.txtBoxY22.PasswordChar = '\0';
            this.txtBoxY22.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoxY22.SelectedText = "";
            this.txtBoxY22.SelectionLength = 0;
            this.txtBoxY22.SelectionStart = 0;
            this.txtBoxY22.ShortcutsEnabled = true;
            this.txtBoxY22.Size = new System.Drawing.Size(52, 23);
            this.txtBoxY22.TabIndex = 5;
            this.txtBoxY22.Text = "Y2";
            this.txtBoxY22.UseSelectable = true;
            this.txtBoxY22.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoxY22.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBoxY22.Click += new System.EventHandler(this.TxtBoxOnClick);
            // 
            // txtBoxX2
            // 
            // 
            // 
            // 
            this.txtBoxX2.CustomButton.Image = null;
            this.txtBoxX2.CustomButton.Location = new System.Drawing.Point(30, 1);
            this.txtBoxX2.CustomButton.Name = "";
            this.txtBoxX2.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoxX2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoxX2.CustomButton.TabIndex = 1;
            this.txtBoxX2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoxX2.CustomButton.UseSelectable = true;
            this.txtBoxX2.CustomButton.Visible = false;
            this.txtBoxX2.Lines = new string[] {
        "X2"};
            this.txtBoxX2.Location = new System.Drawing.Point(76, 167);
            this.txtBoxX2.MaxLength = 32767;
            this.txtBoxX2.Name = "txtBoxX2";
            this.txtBoxX2.PasswordChar = '\0';
            this.txtBoxX2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoxX2.SelectedText = "";
            this.txtBoxX2.SelectionLength = 0;
            this.txtBoxX2.SelectionStart = 0;
            this.txtBoxX2.ShortcutsEnabled = true;
            this.txtBoxX2.Size = new System.Drawing.Size(52, 23);
            this.txtBoxX2.TabIndex = 4;
            this.txtBoxX2.Text = "X2";
            this.txtBoxX2.UseSelectable = true;
            this.txtBoxX2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoxX2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBoxX2.Click += new System.EventHandler(this.TxtBoxOnClick);
            // 
            // txtBoxZ3
            // 
            // 
            // 
            // 
            this.txtBoxZ3.CustomButton.Image = null;
            this.txtBoxZ3.CustomButton.Location = new System.Drawing.Point(30, 1);
            this.txtBoxZ3.CustomButton.Name = "";
            this.txtBoxZ3.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoxZ3.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoxZ3.CustomButton.TabIndex = 1;
            this.txtBoxZ3.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoxZ3.CustomButton.UseSelectable = true;
            this.txtBoxZ3.CustomButton.Visible = false;
            this.txtBoxZ3.Lines = new string[] {
        "Z3"};
            this.txtBoxZ3.Location = new System.Drawing.Point(240, 214);
            this.txtBoxZ3.MaxLength = 32767;
            this.txtBoxZ3.Name = "txtBoxZ3";
            this.txtBoxZ3.PasswordChar = '\0';
            this.txtBoxZ3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoxZ3.SelectedText = "";
            this.txtBoxZ3.SelectionLength = 0;
            this.txtBoxZ3.SelectionStart = 0;
            this.txtBoxZ3.ShortcutsEnabled = true;
            this.txtBoxZ3.Size = new System.Drawing.Size(52, 23);
            this.txtBoxZ3.TabIndex = 9;
            this.txtBoxZ3.Text = "Z3";
            this.txtBoxZ3.UseSelectable = true;
            this.txtBoxZ3.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoxZ3.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBoxZ3.Click += new System.EventHandler(this.TxtBoxOnClick);
            // 
            // txtBoxY3
            // 
            // 
            // 
            // 
            this.txtBoxY3.CustomButton.Image = null;
            this.txtBoxY3.CustomButton.Location = new System.Drawing.Point(30, 1);
            this.txtBoxY3.CustomButton.Name = "";
            this.txtBoxY3.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoxY3.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoxY3.CustomButton.TabIndex = 1;
            this.txtBoxY3.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoxY3.CustomButton.UseSelectable = true;
            this.txtBoxY3.CustomButton.Visible = false;
            this.txtBoxY3.Lines = new string[] {
        "Y3"};
            this.txtBoxY3.Location = new System.Drawing.Point(158, 214);
            this.txtBoxY3.MaxLength = 32767;
            this.txtBoxY3.Name = "txtBoxY3";
            this.txtBoxY3.PasswordChar = '\0';
            this.txtBoxY3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoxY3.SelectedText = "";
            this.txtBoxY3.SelectionLength = 0;
            this.txtBoxY3.SelectionStart = 0;
            this.txtBoxY3.ShortcutsEnabled = true;
            this.txtBoxY3.Size = new System.Drawing.Size(52, 23);
            this.txtBoxY3.TabIndex = 8;
            this.txtBoxY3.Text = "Y3";
            this.txtBoxY3.UseSelectable = true;
            this.txtBoxY3.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoxY3.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBoxY3.Click += new System.EventHandler(this.TxtBoxOnClick);
            // 
            // txtBoxX3
            // 
            // 
            // 
            // 
            this.txtBoxX3.CustomButton.Image = null;
            this.txtBoxX3.CustomButton.Location = new System.Drawing.Point(30, 1);
            this.txtBoxX3.CustomButton.Name = "";
            this.txtBoxX3.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoxX3.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoxX3.CustomButton.TabIndex = 1;
            this.txtBoxX3.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoxX3.CustomButton.UseSelectable = true;
            this.txtBoxX3.CustomButton.Visible = false;
            this.txtBoxX3.Lines = new string[] {
        "X3"};
            this.txtBoxX3.Location = new System.Drawing.Point(76, 214);
            this.txtBoxX3.MaxLength = 32767;
            this.txtBoxX3.Name = "txtBoxX3";
            this.txtBoxX3.PasswordChar = '\0';
            this.txtBoxX3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoxX3.SelectedText = "";
            this.txtBoxX3.SelectionLength = 0;
            this.txtBoxX3.SelectionStart = 0;
            this.txtBoxX3.ShortcutsEnabled = true;
            this.txtBoxX3.Size = new System.Drawing.Size(52, 23);
            this.txtBoxX3.TabIndex = 7;
            this.txtBoxX3.Text = "X3";
            this.txtBoxX3.UseSelectable = true;
            this.txtBoxX3.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoxX3.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBoxX3.Click += new System.EventHandler(this.TxtBoxOnClick);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(134, 121);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(18, 19);
            this.metroLabel1.TabIndex = 10;
            this.metroLabel1.Text = "+";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(134, 171);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(18, 19);
            this.metroLabel2.TabIndex = 11;
            this.metroLabel2.Text = "+";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(134, 214);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(18, 19);
            this.metroLabel3.TabIndex = 12;
            this.metroLabel3.Text = "+";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(216, 214);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(18, 19);
            this.metroLabel4.TabIndex = 15;
            this.metroLabel4.Text = "+";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(216, 171);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(18, 19);
            this.metroLabel5.TabIndex = 14;
            this.metroLabel5.Text = "+";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(216, 121);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(18, 19);
            this.metroLabel6.TabIndex = 13;
            this.metroLabel6.Text = "+";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(298, 117);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(18, 19);
            this.metroLabel7.TabIndex = 16;
            this.metroLabel7.Text = "=";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(298, 171);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(18, 19);
            this.metroLabel8.TabIndex = 17;
            this.metroLabel8.Text = "=";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(298, 214);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(18, 19);
            this.metroLabel9.TabIndex = 18;
            this.metroLabel9.Text = "=";
            // 
            // txtBoxResult3
            // 
            // 
            // 
            // 
            this.txtBoxResult3.CustomButton.Image = null;
            this.txtBoxResult3.CustomButton.Location = new System.Drawing.Point(30, 1);
            this.txtBoxResult3.CustomButton.Name = "";
            this.txtBoxResult3.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoxResult3.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoxResult3.CustomButton.TabIndex = 1;
            this.txtBoxResult3.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoxResult3.CustomButton.UseSelectable = true;
            this.txtBoxResult3.CustomButton.Visible = false;
            this.txtBoxResult3.Lines = new string[] {
        "Result"};
            this.txtBoxResult3.Location = new System.Drawing.Point(322, 214);
            this.txtBoxResult3.MaxLength = 32767;
            this.txtBoxResult3.Name = "txtBoxResult3";
            this.txtBoxResult3.PasswordChar = '\0';
            this.txtBoxResult3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoxResult3.SelectedText = "";
            this.txtBoxResult3.SelectionLength = 0;
            this.txtBoxResult3.SelectionStart = 0;
            this.txtBoxResult3.ShortcutsEnabled = true;
            this.txtBoxResult3.Size = new System.Drawing.Size(52, 23);
            this.txtBoxResult3.TabIndex = 21;
            this.txtBoxResult3.Text = "Result";
            this.txtBoxResult3.UseSelectable = true;
            this.txtBoxResult3.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoxResult3.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBoxResult3.Click += new System.EventHandler(this.TxtBoxOnClick);
            // 
            // txtBoxResult2
            // 
            // 
            // 
            // 
            this.txtBoxResult2.CustomButton.Image = null;
            this.txtBoxResult2.CustomButton.Location = new System.Drawing.Point(30, 1);
            this.txtBoxResult2.CustomButton.Name = "";
            this.txtBoxResult2.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoxResult2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoxResult2.CustomButton.TabIndex = 1;
            this.txtBoxResult2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoxResult2.CustomButton.UseSelectable = true;
            this.txtBoxResult2.CustomButton.Visible = false;
            this.txtBoxResult2.Lines = new string[] {
        "Result"};
            this.txtBoxResult2.Location = new System.Drawing.Point(322, 167);
            this.txtBoxResult2.MaxLength = 32767;
            this.txtBoxResult2.Name = "txtBoxResult2";
            this.txtBoxResult2.PasswordChar = '\0';
            this.txtBoxResult2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoxResult2.SelectedText = "";
            this.txtBoxResult2.SelectionLength = 0;
            this.txtBoxResult2.SelectionStart = 0;
            this.txtBoxResult2.ShortcutsEnabled = true;
            this.txtBoxResult2.Size = new System.Drawing.Size(52, 23);
            this.txtBoxResult2.TabIndex = 20;
            this.txtBoxResult2.Text = "Result";
            this.txtBoxResult2.UseSelectable = true;
            this.txtBoxResult2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoxResult2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBoxResult2.Click += new System.EventHandler(this.TxtBoxOnClick);
            // 
            // txtBoxResult1
            // 
            // 
            // 
            // 
            this.txtBoxResult1.CustomButton.Image = null;
            this.txtBoxResult1.CustomButton.Location = new System.Drawing.Point(30, 1);
            this.txtBoxResult1.CustomButton.Name = "";
            this.txtBoxResult1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoxResult1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoxResult1.CustomButton.TabIndex = 1;
            this.txtBoxResult1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoxResult1.CustomButton.UseSelectable = true;
            this.txtBoxResult1.CustomButton.Visible = false;
            this.txtBoxResult1.Lines = new string[] {
        "Result"};
            this.txtBoxResult1.Location = new System.Drawing.Point(322, 117);
            this.txtBoxResult1.MaxLength = 32767;
            this.txtBoxResult1.Name = "txtBoxResult1";
            this.txtBoxResult1.PasswordChar = '\0';
            this.txtBoxResult1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoxResult1.SelectedText = "";
            this.txtBoxResult1.SelectionLength = 0;
            this.txtBoxResult1.SelectionStart = 0;
            this.txtBoxResult1.ShortcutsEnabled = true;
            this.txtBoxResult1.Size = new System.Drawing.Size(52, 23);
            this.txtBoxResult1.TabIndex = 19;
            this.txtBoxResult1.Text = "Result";
            this.txtBoxResult1.UseSelectable = true;
            this.txtBoxResult1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoxResult1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtBoxResult1.Click += new System.EventHandler(this.TxtBoxOnClick);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(549, 214);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(30, 19);
            this.metroLabel10.TabIndex = 24;
            this.metroLabel10.Text = "Z =";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(549, 171);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(30, 19);
            this.metroLabel11.TabIndex = 23;
            this.metroLabel11.Text = "Y =";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(549, 121);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(34, 19);
            this.metroLabel12.TabIndex = 22;
            this.metroLabel12.Text = "X = ";
            // 
            // txtLabelZ
            // 
            this.txtLabelZ.AutoSize = true;
            this.txtLabelZ.Location = new System.Drawing.Point(585, 214);
            this.txtLabelZ.Name = "txtLabelZ";
            this.txtLabelZ.Size = new System.Drawing.Size(58, 19);
            this.txtLabelZ.TabIndex = 27;
            this.txtLabelZ.Text = "Number";
            // 
            // txtLabelY
            // 
            this.txtLabelY.AutoSize = true;
            this.txtLabelY.Location = new System.Drawing.Point(585, 171);
            this.txtLabelY.Name = "txtLabelY";
            this.txtLabelY.Size = new System.Drawing.Size(58, 19);
            this.txtLabelY.TabIndex = 26;
            this.txtLabelY.Text = "Number";
            // 
            // txtLabelX
            // 
            this.txtLabelX.AutoSize = true;
            this.txtLabelX.Location = new System.Drawing.Point(585, 121);
            this.txtLabelX.Name = "txtLabelX";
            this.txtLabelX.Size = new System.Drawing.Size(58, 19);
            this.txtLabelX.TabIndex = 25;
            this.txtLabelX.Text = "Number";
            // 
            // EcuationCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtLabelZ);
            this.Controls.Add(this.txtLabelY);
            this.Controls.Add(this.txtLabelX);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.metroLabel12);
            this.Controls.Add(this.txtBoxResult3);
            this.Controls.Add(this.txtBoxResult2);
            this.Controls.Add(this.txtBoxResult1);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.txtBoxZ3);
            this.Controls.Add(this.txtBoxY3);
            this.Controls.Add(this.txtBoxX3);
            this.Controls.Add(this.txtBoxZ2);
            this.Controls.Add(this.txtBoxY22);
            this.Controls.Add(this.txtBoxX2);
            this.Controls.Add(this.txtBoxZ1);
            this.Controls.Add(this.txtBoxY1);
            this.Controls.Add(this.txtBoxX1);
            this.Controls.Add(this.metroButton1);
            this.Name = "EcuationCalculator";
            this.Text = "Number";
            this.Load += new System.EventHandler(this.EcuationCalculator_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroTextBox txtBoxX1;
        private MetroFramework.Controls.MetroTextBox txtBoxY1;
        private MetroFramework.Controls.MetroTextBox txtBoxZ1;
        private MetroFramework.Controls.MetroTextBox txtBoxZ2;
        private MetroFramework.Controls.MetroTextBox txtBoxY22;
        private MetroFramework.Controls.MetroTextBox txtBoxX2;
        private MetroFramework.Controls.MetroTextBox txtBoxZ3;
        private MetroFramework.Controls.MetroTextBox txtBoxY3;
        private MetroFramework.Controls.MetroTextBox txtBoxX3;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroTextBox txtBoxResult3;
        private MetroFramework.Controls.MetroTextBox txtBoxResult2;
        private MetroFramework.Controls.MetroTextBox txtBoxResult1;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel txtLabelZ;
        private MetroFramework.Controls.MetroLabel txtLabelY;
        private MetroFramework.Controls.MetroLabel txtLabelX;
    }
}

