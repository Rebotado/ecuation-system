﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EcuationSystemWF.Model;

namespace EcuationSystemWF.View
{
    public interface IMainView
    {
        event EventHandler onResult;

        double X1 { get;  }
        double X2 { get;  }
        double X3 { get;  }
        double Y1 { get;  }
        double Y2 { get;  }
        double Y3 { get;  }
        double Z1 { get;  }
        double Z2 { get;  }
        double Z3 { get; }
        string Output {set; }

        VectorSystem GetSystem();
        void Result(bool dependency);
    }
}
