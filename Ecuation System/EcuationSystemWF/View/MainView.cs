﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EcuationSystemWF.Model;
using MetroFramework.Forms;

namespace EcuationSystemWF.View
{
    public partial class MainView : MetroForm, IMainView
    {
        public event EventHandler onResult;
        public MainView()
        {
            InitializeComponent();
        }


        public double X1 { get { return Convert.ToDouble(x1TextBox.Text); } }
        public double X2 { get { return Convert.ToDouble(x2TextBox.Text); } }
        public double X3 { get { return Convert.ToDouble(x3TextBox.Text); } }
        public double Y1 { get { return Convert.ToDouble(y1TextBox.Text); } }
        public double Y2 { get { return Convert.ToDouble(y2TextBox.Text); } }
        public double Y3 { get { return Convert.ToDouble(y3TextBox.Text); } }
        public double Z1 { get { return Convert.ToDouble(z1TextBox.Text); } }
        public double Z2 { get { return Convert.ToDouble(z2TextBox.Text); } }
        public double Z3 { get { return Convert.ToDouble(z3TextBox.Text); } }
        public string Output { set { resultTextBox.Text = value; } }


        public void Result(bool dependency)
        {
            if (dependency)
                Output = "Dependiente";
            else
                Output = "Independiente";
        }

        private void MainView_Load(object sender, EventArgs e)
        {
            selectorComboBox.SelectedIndex = 1;
        }

        public VectorSystem GetSystem()
        {
            if (selectorComboBox.Text == "3x3")
            {
                return new VectorSystem
                {
                    V1 = new Vector(X1, Y1, Z1),
                    V2 = new Vector(X2, Y2, Z2),
                    V3 = new Vector(X3, Y3, Z3)
                };
            }
            else
            {
                return new VectorSystem
                {
                    V1 = new Vector(X1, Y1),
                    V2 = new Vector(X2, Y2)
                };
            }
        }

        private void ResultButton_Click(object sender, EventArgs e)
        {
            onResult(sender, e);
        }

        private void SelectorComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if(selectorComboBox.Text == "2x2")
            {
                x3TextBox.Hide();
                y3TextBox.Hide();
                z3TextBox.Hide();
                z1TextBox.Hide();
                z2TextBox.Hide();
            }
            else
            {
                x3TextBox.Show();
                y3TextBox.Show();
                z3TextBox.Show();
                z1TextBox.Show();
                z2TextBox.Show();
            }
        }
    }
}
