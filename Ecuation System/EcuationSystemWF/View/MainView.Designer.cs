﻿namespace EcuationSystemWF.View
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.x1TextBox = new MetroFramework.Controls.MetroTextBox();
            this.y1TextBox = new MetroFramework.Controls.MetroTextBox();
            this.z1TextBox = new MetroFramework.Controls.MetroTextBox();
            this.z2TextBox = new MetroFramework.Controls.MetroTextBox();
            this.y2TextBox = new MetroFramework.Controls.MetroTextBox();
            this.x2TextBox = new MetroFramework.Controls.MetroTextBox();
            this.z3TextBox = new MetroFramework.Controls.MetroTextBox();
            this.y3TextBox = new MetroFramework.Controls.MetroTextBox();
            this.x3TextBox = new MetroFramework.Controls.MetroTextBox();
            this.resultButton = new System.Windows.Forms.Button();
            this.resultTextBox = new MetroFramework.Controls.MetroTextBox();
            this.selectorComboBox = new MetroFramework.Controls.MetroComboBox();
            this.SuspendLayout();
            // 
            // x1TextBox
            // 
            // 
            // 
            // 
            this.x1TextBox.CustomButton.Image = null;
            this.x1TextBox.CustomButton.Location = new System.Drawing.Point(53, 1);
            this.x1TextBox.CustomButton.Name = "";
            this.x1TextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.x1TextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.x1TextBox.CustomButton.TabIndex = 1;
            this.x1TextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.x1TextBox.CustomButton.UseSelectable = true;
            this.x1TextBox.CustomButton.Visible = false;
            this.x1TextBox.Lines = new string[] {
        "1"};
            this.x1TextBox.Location = new System.Drawing.Point(297, 128);
            this.x1TextBox.MaxLength = 32767;
            this.x1TextBox.Name = "x1TextBox";
            this.x1TextBox.PasswordChar = '\0';
            this.x1TextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.x1TextBox.SelectedText = "";
            this.x1TextBox.SelectionLength = 0;
            this.x1TextBox.SelectionStart = 0;
            this.x1TextBox.ShortcutsEnabled = true;
            this.x1TextBox.Size = new System.Drawing.Size(75, 23);
            this.x1TextBox.TabIndex = 0;
            this.x1TextBox.Text = "1";
            this.x1TextBox.UseSelectable = true;
            this.x1TextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.x1TextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // y1TextBox
            // 
            // 
            // 
            // 
            this.y1TextBox.CustomButton.Image = null;
            this.y1TextBox.CustomButton.Location = new System.Drawing.Point(53, 1);
            this.y1TextBox.CustomButton.Name = "";
            this.y1TextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.y1TextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.y1TextBox.CustomButton.TabIndex = 1;
            this.y1TextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.y1TextBox.CustomButton.UseSelectable = true;
            this.y1TextBox.CustomButton.Visible = false;
            this.y1TextBox.Lines = new string[] {
        "1"};
            this.y1TextBox.Location = new System.Drawing.Point(392, 128);
            this.y1TextBox.MaxLength = 32767;
            this.y1TextBox.Name = "y1TextBox";
            this.y1TextBox.PasswordChar = '\0';
            this.y1TextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.y1TextBox.SelectedText = "";
            this.y1TextBox.SelectionLength = 0;
            this.y1TextBox.SelectionStart = 0;
            this.y1TextBox.ShortcutsEnabled = true;
            this.y1TextBox.Size = new System.Drawing.Size(75, 23);
            this.y1TextBox.TabIndex = 1;
            this.y1TextBox.Text = "1";
            this.y1TextBox.UseSelectable = true;
            this.y1TextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.y1TextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // z1TextBox
            // 
            // 
            // 
            // 
            this.z1TextBox.CustomButton.Image = null;
            this.z1TextBox.CustomButton.Location = new System.Drawing.Point(53, 1);
            this.z1TextBox.CustomButton.Name = "";
            this.z1TextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.z1TextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.z1TextBox.CustomButton.TabIndex = 1;
            this.z1TextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.z1TextBox.CustomButton.UseSelectable = true;
            this.z1TextBox.CustomButton.Visible = false;
            this.z1TextBox.Lines = new string[] {
        "1"};
            this.z1TextBox.Location = new System.Drawing.Point(484, 128);
            this.z1TextBox.MaxLength = 32767;
            this.z1TextBox.Name = "z1TextBox";
            this.z1TextBox.PasswordChar = '\0';
            this.z1TextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.z1TextBox.SelectedText = "";
            this.z1TextBox.SelectionLength = 0;
            this.z1TextBox.SelectionStart = 0;
            this.z1TextBox.ShortcutsEnabled = true;
            this.z1TextBox.Size = new System.Drawing.Size(75, 23);
            this.z1TextBox.TabIndex = 2;
            this.z1TextBox.Text = "1";
            this.z1TextBox.UseSelectable = true;
            this.z1TextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.z1TextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // z2TextBox
            // 
            // 
            // 
            // 
            this.z2TextBox.CustomButton.Image = null;
            this.z2TextBox.CustomButton.Location = new System.Drawing.Point(53, 1);
            this.z2TextBox.CustomButton.Name = "";
            this.z2TextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.z2TextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.z2TextBox.CustomButton.TabIndex = 1;
            this.z2TextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.z2TextBox.CustomButton.UseSelectable = true;
            this.z2TextBox.CustomButton.Visible = false;
            this.z2TextBox.Lines = new string[] {
        "1"};
            this.z2TextBox.Location = new System.Drawing.Point(484, 166);
            this.z2TextBox.MaxLength = 32767;
            this.z2TextBox.Name = "z2TextBox";
            this.z2TextBox.PasswordChar = '\0';
            this.z2TextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.z2TextBox.SelectedText = "";
            this.z2TextBox.SelectionLength = 0;
            this.z2TextBox.SelectionStart = 0;
            this.z2TextBox.ShortcutsEnabled = true;
            this.z2TextBox.Size = new System.Drawing.Size(75, 23);
            this.z2TextBox.TabIndex = 5;
            this.z2TextBox.Text = "1";
            this.z2TextBox.UseSelectable = true;
            this.z2TextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.z2TextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // y2TextBox
            // 
            // 
            // 
            // 
            this.y2TextBox.CustomButton.Image = null;
            this.y2TextBox.CustomButton.Location = new System.Drawing.Point(53, 1);
            this.y2TextBox.CustomButton.Name = "";
            this.y2TextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.y2TextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.y2TextBox.CustomButton.TabIndex = 1;
            this.y2TextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.y2TextBox.CustomButton.UseSelectable = true;
            this.y2TextBox.CustomButton.Visible = false;
            this.y2TextBox.Lines = new string[] {
        "1"};
            this.y2TextBox.Location = new System.Drawing.Point(392, 166);
            this.y2TextBox.MaxLength = 32767;
            this.y2TextBox.Name = "y2TextBox";
            this.y2TextBox.PasswordChar = '\0';
            this.y2TextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.y2TextBox.SelectedText = "";
            this.y2TextBox.SelectionLength = 0;
            this.y2TextBox.SelectionStart = 0;
            this.y2TextBox.ShortcutsEnabled = true;
            this.y2TextBox.Size = new System.Drawing.Size(75, 23);
            this.y2TextBox.TabIndex = 4;
            this.y2TextBox.Text = "1";
            this.y2TextBox.UseSelectable = true;
            this.y2TextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.y2TextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // x2TextBox
            // 
            // 
            // 
            // 
            this.x2TextBox.CustomButton.Image = null;
            this.x2TextBox.CustomButton.Location = new System.Drawing.Point(53, 1);
            this.x2TextBox.CustomButton.Name = "";
            this.x2TextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.x2TextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.x2TextBox.CustomButton.TabIndex = 1;
            this.x2TextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.x2TextBox.CustomButton.UseSelectable = true;
            this.x2TextBox.CustomButton.Visible = false;
            this.x2TextBox.Lines = new string[] {
        "1"};
            this.x2TextBox.Location = new System.Drawing.Point(297, 166);
            this.x2TextBox.MaxLength = 32767;
            this.x2TextBox.Name = "x2TextBox";
            this.x2TextBox.PasswordChar = '\0';
            this.x2TextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.x2TextBox.SelectedText = "";
            this.x2TextBox.SelectionLength = 0;
            this.x2TextBox.SelectionStart = 0;
            this.x2TextBox.ShortcutsEnabled = true;
            this.x2TextBox.Size = new System.Drawing.Size(75, 23);
            this.x2TextBox.TabIndex = 3;
            this.x2TextBox.Text = "1";
            this.x2TextBox.UseSelectable = true;
            this.x2TextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.x2TextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // z3TextBox
            // 
            // 
            // 
            // 
            this.z3TextBox.CustomButton.Image = null;
            this.z3TextBox.CustomButton.Location = new System.Drawing.Point(53, 1);
            this.z3TextBox.CustomButton.Name = "";
            this.z3TextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.z3TextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.z3TextBox.CustomButton.TabIndex = 1;
            this.z3TextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.z3TextBox.CustomButton.UseSelectable = true;
            this.z3TextBox.CustomButton.Visible = false;
            this.z3TextBox.Lines = new string[] {
        "1"};
            this.z3TextBox.Location = new System.Drawing.Point(484, 207);
            this.z3TextBox.MaxLength = 32767;
            this.z3TextBox.Name = "z3TextBox";
            this.z3TextBox.PasswordChar = '\0';
            this.z3TextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.z3TextBox.SelectedText = "";
            this.z3TextBox.SelectionLength = 0;
            this.z3TextBox.SelectionStart = 0;
            this.z3TextBox.ShortcutsEnabled = true;
            this.z3TextBox.Size = new System.Drawing.Size(75, 23);
            this.z3TextBox.TabIndex = 8;
            this.z3TextBox.Text = "1";
            this.z3TextBox.UseSelectable = true;
            this.z3TextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.z3TextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // y3TextBox
            // 
            // 
            // 
            // 
            this.y3TextBox.CustomButton.Image = null;
            this.y3TextBox.CustomButton.Location = new System.Drawing.Point(53, 1);
            this.y3TextBox.CustomButton.Name = "";
            this.y3TextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.y3TextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.y3TextBox.CustomButton.TabIndex = 1;
            this.y3TextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.y3TextBox.CustomButton.UseSelectable = true;
            this.y3TextBox.CustomButton.Visible = false;
            this.y3TextBox.Lines = new string[] {
        "1"};
            this.y3TextBox.Location = new System.Drawing.Point(392, 207);
            this.y3TextBox.MaxLength = 32767;
            this.y3TextBox.Name = "y3TextBox";
            this.y3TextBox.PasswordChar = '\0';
            this.y3TextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.y3TextBox.SelectedText = "";
            this.y3TextBox.SelectionLength = 0;
            this.y3TextBox.SelectionStart = 0;
            this.y3TextBox.ShortcutsEnabled = true;
            this.y3TextBox.Size = new System.Drawing.Size(75, 23);
            this.y3TextBox.TabIndex = 7;
            this.y3TextBox.Text = "1";
            this.y3TextBox.UseSelectable = true;
            this.y3TextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.y3TextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // x3TextBox
            // 
            // 
            // 
            // 
            this.x3TextBox.CustomButton.Image = null;
            this.x3TextBox.CustomButton.Location = new System.Drawing.Point(53, 1);
            this.x3TextBox.CustomButton.Name = "";
            this.x3TextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.x3TextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.x3TextBox.CustomButton.TabIndex = 1;
            this.x3TextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.x3TextBox.CustomButton.UseSelectable = true;
            this.x3TextBox.CustomButton.Visible = false;
            this.x3TextBox.Lines = new string[] {
        "1"};
            this.x3TextBox.Location = new System.Drawing.Point(297, 207);
            this.x3TextBox.MaxLength = 32767;
            this.x3TextBox.Name = "x3TextBox";
            this.x3TextBox.PasswordChar = '\0';
            this.x3TextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.x3TextBox.SelectedText = "";
            this.x3TextBox.SelectionLength = 0;
            this.x3TextBox.SelectionStart = 0;
            this.x3TextBox.ShortcutsEnabled = true;
            this.x3TextBox.Size = new System.Drawing.Size(75, 23);
            this.x3TextBox.TabIndex = 6;
            this.x3TextBox.Text = "1";
            this.x3TextBox.UseSelectable = true;
            this.x3TextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.x3TextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // resultButton
            // 
            this.resultButton.Location = new System.Drawing.Point(392, 255);
            this.resultButton.Name = "resultButton";
            this.resultButton.Size = new System.Drawing.Size(75, 23);
            this.resultButton.TabIndex = 9;
            this.resultButton.Text = "Resultado";
            this.resultButton.UseVisualStyleBackColor = true;
            this.resultButton.Click += new System.EventHandler(this.ResultButton_Click);
            // 
            // resultTextBox
            // 
            // 
            // 
            // 
            this.resultTextBox.CustomButton.Image = null;
            this.resultTextBox.CustomButton.Location = new System.Drawing.Point(89, 1);
            this.resultTextBox.CustomButton.Name = "";
            this.resultTextBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.resultTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.resultTextBox.CustomButton.TabIndex = 1;
            this.resultTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.resultTextBox.CustomButton.UseSelectable = true;
            this.resultTextBox.CustomButton.Visible = false;
            this.resultTextBox.Lines = new string[0];
            this.resultTextBox.Location = new System.Drawing.Point(633, 166);
            this.resultTextBox.MaxLength = 32767;
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.PasswordChar = '\0';
            this.resultTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.resultTextBox.SelectedText = "";
            this.resultTextBox.SelectionLength = 0;
            this.resultTextBox.SelectionStart = 0;
            this.resultTextBox.ShortcutsEnabled = true;
            this.resultTextBox.Size = new System.Drawing.Size(111, 23);
            this.resultTextBox.TabIndex = 10;
            this.resultTextBox.UseSelectable = true;
            this.resultTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.resultTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // selectorComboBox
            // 
            this.selectorComboBox.FormattingEnabled = true;
            this.selectorComboBox.ItemHeight = 23;
            this.selectorComboBox.Items.AddRange(new object[] {
            "3x3",
            "2x2"});
            this.selectorComboBox.Location = new System.Drawing.Point(87, 160);
            this.selectorComboBox.Name = "selectorComboBox";
            this.selectorComboBox.Size = new System.Drawing.Size(121, 29);
            this.selectorComboBox.TabIndex = 11;
            this.selectorComboBox.UseSelectable = true;
            this.selectorComboBox.SelectedValueChanged += new System.EventHandler(this.SelectorComboBox_SelectedValueChanged);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.selectorComboBox);
            this.Controls.Add(this.resultTextBox);
            this.Controls.Add(this.resultButton);
            this.Controls.Add(this.z3TextBox);
            this.Controls.Add(this.y3TextBox);
            this.Controls.Add(this.x3TextBox);
            this.Controls.Add(this.z2TextBox);
            this.Controls.Add(this.y2TextBox);
            this.Controls.Add(this.x2TextBox);
            this.Controls.Add(this.z1TextBox);
            this.Controls.Add(this.y1TextBox);
            this.Controls.Add(this.x1TextBox);
            this.Name = "MainView";
            this.Text = "MainView";
            this.Load += new System.EventHandler(this.MainView_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox x1TextBox;
        private MetroFramework.Controls.MetroTextBox y1TextBox;
        private MetroFramework.Controls.MetroTextBox z1TextBox;
        private MetroFramework.Controls.MetroTextBox z2TextBox;
        private MetroFramework.Controls.MetroTextBox y2TextBox;
        private MetroFramework.Controls.MetroTextBox x2TextBox;
        private MetroFramework.Controls.MetroTextBox z3TextBox;
        private MetroFramework.Controls.MetroTextBox y3TextBox;
        private MetroFramework.Controls.MetroTextBox x3TextBox;
        private System.Windows.Forms.Button resultButton;
        private MetroFramework.Controls.MetroTextBox resultTextBox;
        private MetroFramework.Controls.MetroComboBox selectorComboBox;
    }
}