﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcuationSystemWF.Model
{
    public class VectorSystem
    {
        public Vector V1 { get; set; }
        public Vector V2 { get; set; }
        public Vector V3 { get; set; }

        public bool Dependency()
        {
            if (V3 != null)
            {
                if (V1.Equals(V2) | V1.Equals(V3) | V2.Equals(V3))
                    return true;
                else if (V1.VectorZero() | V2.VectorZero() | V3.VectorZero())
                    return true;
                else if (V1.Scalar(V2, V3))
                    return true;
                else if (EcuationExtensions.GetDeterminant(V1, V2, V3) == 0)
                    return true;
                else
                    return false;
            }
            else if(V2 == null && V3 == null)
            {

            }
            else
            {
                if (V1.Equals(V2))
                    return true;
                else if (V1.Scalar(V2))
                    return true;
                else if (V1.VectorZero() | V2.VectorZero())
                    return true;
                else if (EcuationExtensions.GetDeterminant(V1, V2) == 0)
                    return true;
                else
                    return false;
            }
            return false;
        }


    }
}
