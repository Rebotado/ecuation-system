﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcuationSystemWF.Model
{
    public class Vector
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public Vector(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }
        public Vector(double x, double y)
        {
            X = x;
            Y = y;
        }
        public bool VectorZero()
        {
            if (X == 0 && Y == 0 && Z == 0)
                return true;
            else
                return false;
        }
        public bool Scalar(Vector V1)
        {
            if (V1.X / X == V1.Y / Y)
                return true;
            else
                return false;
        }
        public bool Scalar(Vector V1, Vector V2)
        {
            if ((V1.X / V2.X == V1.Y / V2.Y) && (V1.Z / V2.Z == V1.X / V2.X))
            {
                return true;
            }
            else if (((X / V2.X == Y / V2.Y) && (Z / V2.Z == X / V2.X)))
            {
                return true;
            }
            else if (((X / V1.X == Y / V1.Y) && (Z / V1.Z == X / V1.X)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }




        public bool Equals(Vector V2)
        {
            return (X == V2.X) && (Y == V2.Y) && (Z == V2.Z);
        }
    }
}
