﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using MetroFramework.Controls;

namespace EcuationSystemWF
{
    public partial class EcuationCalculator : MetroForm
    {
        public EcuationCalculator()
        {
            InitializeComponent();
            Text = "Ecuation Solver";
            txtLabelX.Text = "";
            txtLabelY.Text = "";
            txtLabelZ.Text = "";
        }

        private void EcuationCalculator_Load(object sender, EventArgs e)
        {

        }

        private void MetroButton1_Click(object sender, EventArgs e)
        {
            try
            {
                double[,] s = new double[3, 4]
                {
                {Convert.ToDouble(txtBoxX1.Text),Convert.ToDouble(txtBoxY1.Text),Convert.ToDouble(txtBoxZ1.Text),Convert.ToDouble(txtBoxResult1.Text)},
                {Convert.ToDouble(txtBoxX2.Text),Convert.ToDouble(txtBoxY22.Text),Convert.ToDouble(txtBoxZ2.Text),Convert.ToDouble(txtBoxResult2.Text) },
                {Convert.ToDouble(txtBoxX3.Text),Convert.ToDouble(txtBoxY3.Text),Convert.ToDouble(txtBoxZ3.Text),Convert.ToDouble(txtBoxResult3.Text) }
                };
            }
            catch (Exception)
            {
                MessageBox.Show("Error, you need to use either integers or decimals.");
                return;
            }
            double[,] ecuationSystem = new double[3, 4]
{
                {Convert.ToDouble(txtBoxX1.Text),Convert.ToDouble(txtBoxY1.Text),Convert.ToDouble(txtBoxZ1.Text),Convert.ToDouble(txtBoxResult1.Text)},
                {Convert.ToDouble(txtBoxX2.Text),Convert.ToDouble(txtBoxY22.Text),Convert.ToDouble(txtBoxZ2.Text),Convert.ToDouble(txtBoxResult2.Text) },
                {Convert.ToDouble(txtBoxX3.Text),Convert.ToDouble(txtBoxY3.Text),Convert.ToDouble(txtBoxZ3.Text),Convert.ToDouble(txtBoxResult3.Text) }
};

            Dictionary<char, double> determinants = new Dictionary<char, double>()
            {
              {'x' , 0},
              {'y' , 0},
              {'z' , 0},
              {'k' , 0}
            };

            determinants['k'] = EcuationExtensions.GetDeterminant(ecuationSystem);
            determinants['x'] = EcuationExtensions.GetDeterminant(EcuationExtensions.SwapColumns(ecuationSystem, 0, 3));
            determinants['y'] = EcuationExtensions.GetDeterminant(EcuationExtensions.SwapColumns(ecuationSystem, 1, 3));
            determinants['z'] = EcuationExtensions.GetDeterminant(EcuationExtensions.SwapColumns(ecuationSystem, 2, 3));
            double valueX = determinants['x'] / determinants['k'];
            double valueY = determinants['y'] / determinants['k'];
            double valueZ = determinants['z'] / determinants['k'];

            txtLabelX.Text = Convert.ToString(valueX);
            txtLabelY.Text = Convert.ToString(valueY);
            txtLabelZ.Text = Convert.ToString(valueZ);

        }

        private void TxtBoxOnClick(object sender, EventArgs e)
        {
            (sender as MetroTextBox).Text = string.Empty;
        }
    }
}
