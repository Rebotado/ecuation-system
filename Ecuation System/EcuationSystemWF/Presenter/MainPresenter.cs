﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EcuationSystemWF.View;
using EcuationSystemWF.Model;
using System.Diagnostics;

namespace EcuationSystemWF.Presenter
{
    public class MainPresenter
    {
        private readonly IMainView view;
        private VectorSystem model;

        public MainPresenter(IMainView view, VectorSystem model)
        {
            this.view = view;
            this.model = model;

            view.onResult += onResult;
        }

        public void onResult(object sender, EventArgs e)
        {
            model = view.GetSystem();
            bool result = model.Dependency();
            view.Result(result);
        }
    }
}
