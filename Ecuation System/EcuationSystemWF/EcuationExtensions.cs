﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using EcuationSystemWF.Model;

namespace EcuationSystemWF
{
    public class EcuationExtensions
    {

        public static double GetDeterminant(Vector V1, Vector V2)
        {
            return (V1.X + V2.Y) - (V2.X + V1.Y);
        }

        public static double GetDeterminant(Vector V1, Vector V2, Vector V3)
        {
            return ((V1.X + V2.Y + V3.Z) + (V1.Y + V2.Z + V3.X) + (V1.Z + V2.X + V3.Y))
                - ((V3.X - V2.Y + V1.Z) + (V3.Y +  V2.Z + V1.X) + (V3.Z - V2.X + V1.Y));
        }


        public static double GetDeterminant(double[,] ecuationSystem)
        {
            double positiveD = (ecuationSystem[0, 0] * ecuationSystem[1, 1] * ecuationSystem[2, 2]) +
                (ecuationSystem[0, 1] * ecuationSystem[1, 2] * ecuationSystem[2, 0]) +
                (ecuationSystem[0, 2] * ecuationSystem[1, 0] * ecuationSystem[2, 1]);
            double negativeD = (ecuationSystem[2, 0] * ecuationSystem[1, 1] * ecuationSystem[0, 2]) +
                (ecuationSystem[2, 1] * ecuationSystem[1, 2] * ecuationSystem[0, 0]) +
                (ecuationSystem[2, 2] * ecuationSystem[1, 0] * ecuationSystem[0, 1]);

            return positiveD - negativeD;
        }

        public static void DebugMatrix(double[,] ecuationSystem)
        {
            for (int row = 0; row < ecuationSystem.GetLength(0); row++)
            {
                for (int column = 0; column < ecuationSystem.GetLength(1); column++)
                {
                    Debug.Write(ecuationSystem[row, column] + "  ");
                }
                Debug.WriteLine("\n");
            }
        }
        public static double[,] SwapColumns(double[,] ecuationSystem, int column, int columnSwap)
        {
            double[,] tempEcuation = new double[3, 4];
            double[,] ecuationResult = (double[,])ecuationSystem.Clone();
            tempEcuation = (double[,])ecuationSystem.Clone();

            for (int i = 0; i < ecuationResult.GetLength(0); i++)
            {
                ecuationResult[i, column] = tempEcuation[i, columnSwap];
            }
            return ecuationResult;
        }
    }
}
